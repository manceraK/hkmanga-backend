#! /bin/python


import requests, time
from pprint import pprint
from random import randint
from uuslug import slugify

tmo_url = 'http://www.tumangaonline.com/'
tmo_manga_url = 'api/v1/mangas?itemsPerPage=500&page='
total_pages = 0
current_page = 1
authors_set = set()
artists_set = set()
categories_set = set()
demography_set = set()
genres_set = set()
magazines_set = set()
types_set = set()
imgs_arr = []
mangas_arr = []

# Association list

series_artist = []
series_authors = []
series_genres = []

manga_id = 0

timestamp = time.time()
randhash = randint(0, 1000)

print('Identifier: {0}_{1}'.format(str(timestamp), str(randhash)))

class Img():
    manga = ''
    url = ''

class Author():
    id = 0
    name = ''


class Artist():
    id = 0
    name = ''

class Genre():
    id=0
    dsc = ''


class Serie():
    id = 0
    name = ''
    alt_names = []
    synopsis = ''
    start_date = ''
    end_date = ''
    demography = ''
    status = 'No definido'
    manga_type = ''
    category = ''
    magazine = 0
    adult_content = 0
    cover = ''
    periodicity = ''
    score = 0.0
    external_id = 0
    slug = ''

    def __init__(self, id):
        self.id = id
        self.authors = []
        self.artists = []
        self.genres = []


def getTotalPages():
    url_request  = tmo_url + tmo_manga_url + "1"
    print(url_request)
    r = requests.get(url_request)
    json = r.json()
    return int(json["last_page"])


def getJsonData(page):
    r = requests.get(tmo_url + tmo_manga_url + str(page))
    json = r.json()
    total_pages = int(json["last_page"])
    return json["data"]


total_pages = getTotalPages()

for page in range(1, total_pages + 1):
        print(str(page) + " - " + str(manga_id))
        pagedMangas = getJsonData(page)
        for manga in pagedMangas:
            #print("page: ", page, "manga: ", manga["nombre"])
            manga_id += 1
            new_manga = Serie(manga_id)

            artists = manga["artistas"]
            authors = manga["autores"]
            categories = manga["categorias"]
            demography = manga["demografia"]
            genres = manga["generos"]
            status = manga["estado"]
            name = manga["nombre"]
            alt_names = manga["nombres_alternativos"]
            periodicity = manga["periodicidad"]
            magazines = manga["revistas"]
            manga_type = manga["tipo"]
            score = float(manga["puntuacion"])
            synopsis = manga["info"]["sinopsis"]
            is_adult = manga["adulto"]
            cover = manga["imageUrl"]
            publish_date = manga["info"]["fechaCreacion"]
            external_id = manga["id"]
            category = manga["categorias"]


            for artist in artists:
                curr_artist = str(artist["id"]) + "__" + str(artist["artista"])
                artists_set.add(curr_artist)

            for author in authors:
                curr_author = str(author["id"]) + "__"+ str(author["autor"])
                authors_set.add(curr_author)

            for genre in genres:
                curr_genre = str(genre["id"]) + "__" + str(genre["genero"])
                genres_set.add(curr_genre)

            for magazine in magazines:
                curr_magazine = str(magazine["id"]) + "__" + str(magazine["revista"])
                magazines_set.add(curr_magazine)

            alt_names_list = []

            if alt_names:
                notlist = True

            for altname in alt_names:
                tyoe = type(altname["nombre"])
                if len(altname["nombre"]) > 0:
                    alt_names_list.append(altname["nombre"])

            types_set.add(manga_type)
            demography_set.add(demography)

            if (len(category) > 0):
                cat = category[0]["categoria"]
                categories_set.add(cat)
            else:
                cat = 'NULL'

            new_manga.name = name
            new_manga.demography = demography
            new_manga.status = status
            new_manga.alt_names = ", ".join(str(x) for x in alt_names_list)
            new_manga.category = cat
            new_manga.periodicity = periodicity
            new_manga.manga_type = manga_type
            new_manga.score = score
            new_manga.synopsis = synopsis
            new_manga.adult_content = is_adult
            new_manga.authors = authors
            new_manga.artists = artists
            new_manga.genres = genres
            new_manga.magazine = magazines
            new_manga.start_date = publish_date
            new_manga.cover = cover
            new_manga.external_id = external_id
            new_manga.slug = slugify(name, max_length=50)

            mangas_arr.append(new_manga)


def saveListToFile(name, thelist):
    with open(name, 'w') as file:
        for item in thelist:
            file.write(" INSERT ({});\n".format(item))

def save_series_authors(id, authors):
    save_name = 'seriesAuthors_' + str(timestamp) +  '_' + str(randhash) + '.txt'
    for author in authors:
        entry = "INSERT IGNORE INTO manga_series_authors (`series_id`, `author_id`) VALUES(" + str(id) + ", " + str(author["id"]) + ");"
        with open(save_name, 'a') as file:
            file.write(entry + "\n")


def save_series_artist(id, artists):
    save_name = 'seriesArtist_' + str(timestamp) +  '_' + str(randhash) + '.txt'
    for artist in artists:
        entry = "INSERT IGNORE INTO manga_series_artists (`series_id`, `artist_id`) VALUES(" + str(id) + ", " + str(artist["id"]) + ");"
        with open(save_name, 'a') as file:
            file.write(entry + "\n")


def save_series_genres(id, genres):
    save_name = 'seriesGenres_' + str(timestamp) +  '_' + str(randhash) + '.txt'
    for genre in genres:
        entry = "INSERT IGNORE INTO manga_series_genres (`series_id`, `genre_id`) VALUES(" + str(id) + ", " + str(genre["id"]) + ");"
        with open(save_name, 'a') as file:
            file.write(entry + "\n")


def save_series_magazine(id, magazines):
    save_name = 'seriesMagazine_' + str(timestamp) +  '_' + str(randhash) + '.txt'
    for magazine in magazines:
        entry = "INSERT IGNORE INTO manga_series_magazine(`series_id`, `magazine_id`) VALUES(" + str(id) + ", " + str(magazine["id"]) + ");"
        with open(save_name, 'a') as file:
            file.write(entry + "\n")


def get_demograpy_id(demography):
    return {
        'Shounen': 1,
        'Shoujo': 2,
        'Seinen': 3,
        'Josei': 4,
        'Kodomo': 5,
        'Otros': 6
    }.get(demography, 'NULL')


def get_manga_type_id(type):
    return {
        'MANGA': 1,
        'NOVELA': 2,
        'MANHWA': 3,
        'MANHUA': 4,
        'PROPIO': 5,
        'OTRO': 6
    }.get(type, 'NULL')


def get_category_id(type):
    return {
        'One-Shot': 1,
        'Dōjinshi': 2,
        'Yonkoma': 3,
        'Webtoon': 4
    }.get(type, 'NULL')


def save_series(manga):
    save_name ='mangaList_' + str(timestamp) + '_' + str(randhash) + '.sql'

    id = manga.id
    name = manga.name.replace("'","''")
    slug = manga.slug

    if (len(manga.alt_names) > 0):
        alt_names = manga.alt_names.replace("'","''")
    else:
        alt_names = 'NULL'

    if (len(str(manga.synopsis)) > 0):
        synopsis = manga.synopsis.replace("'","''")
    else:
        synopsis = 'Sin descripción'

    start_date = '1900-01-01'
    end_date = '1900-01-01'

    if (len(str(manga.start_date)) > 0):
        start_date = manga.start_date
    else:
        start_date = 'NULL'

    if (len(manga.end_date) > 0):
        end_date = manga.end_date
    else:
        end_date = 'NULL'

    added_date = 'NOW()'

    if (manga.adult_content != '0'):
        adult_content = 1
    else:
        adult_content = 0

    if (len(manga.cover) > 0):
        uri = manga.cover
    else:
        uri = 'NULL'

    url = 'NULL'

    if (len(manga.demography) > 0):
        demography_id = get_demograpy_id(manga.demography)
    else:
        demography_id = 'NULL'

    if (len(manga.magazine) > 0):
        magazine_id = manga.magazine[0]["id"]
    else:
        magazine_id = 'NULL'

    if (len(manga.status) > 0):
        status = manga.status
    else:
        status = 'NULL'

    if (len(manga.manga_type) > 0):
        type_id = get_manga_type_id(manga.manga_type)
    else:
        type_id = 'NULL'

    if (len(manga.category) > 0):
        category = get_category_id(manga.category)
    else:
        category = 'NULL'

    external_id = manga.external_id


    manga_entry = "INSERT IGNORE INTO manga_series (`id`, `name`,`alt_names`,`synopsis`,`start_date`,`end_date`,`added_date`," \
                  "`adult_content`,`uri`,`url`,`demography_id`,`magazine_id`,`status`,`type_id`, `category_id`, `slug`, `external_id`) " \
                  "VALUES({0}, '{1}', '{2}', '{3}', '{4}', '{5}', {6}, {7}, '{8}', '{9}', {10}, {11}, '{12}', {13}, {14}, '{15}', {16});"\
                    .format(id, name, alt_names, synopsis, start_date, end_date, added_date, adult_content, uri, url, demography_id, magazine_id, status, type_id, category, slug, external_id)

    with open(save_name, 'a') as file:
        file.write(manga_entry + "\n")



def saveMangas():
    for manga in mangas_arr:
        save_series_authors(manga.id, manga.authors)
        save_series_artist(manga.id, manga.artists)
        save_series_genres(manga.id, manga.genres)
        save_series(manga)

        # save_name ='mangaList_' + str(timestamp) + '_' + str(randhash) + '.txt'
        # manga_entry = str(manga.name) + ',' + str(manga.alt_names) + ',' + str(manga.start_date) + ',@@@' + str(manga.synopsis) + '@@@,' + str(manga.authors) + ',' + str(manga.artists) + ',' + \
        # str(manga.categories) + ',' + str(manga.demography) + ',' + str(manga.genres) + ',' + str(manga.status) + ',' + str(manga.manga_type) + ',' + str(manga.magazine) + ',' + str(manga.adult_content) + ',' + \
        # str(manga.periodicity) + ',' + str(manga.score) + ',' + str(manga.cover)
        #
        # with open(save_name, 'a') as file:
        #     file.write(manga_entry + "\n\n")

        #with open('external_ids.txt', 'a') as file_external_ids:
            #file_external_ids.write(str(manga.external_id) + "\n")


saveMangas()

manga = mangas_arr[randint(1,5000)]
pprint (vars(manga))


#saveListToFile('autores.txt',list(authors_set))
#saveListToFile('artistas.txt',list(artists_set))
#saveListToFile('cateogrias.txt',list(categories_set))
#saveListToFile('demografias.txt',list(demography_set))
#saveListToFile('generos.txt',list(genres_set))
#saveListToFile('revistas.txt',list(magazines_set))
#saveListToFile('tipos.txt',list(types_set))





