# README #

**HKManga is a work in progress.**

The intention is to develop a manager for manga and comics. The first step is to have a full management API the second step is to develop clients to use the API, the plan is to develop an android app and a website so anyone can manage their series whenever and wherever they want.

It's written on Python+Django.

--

Needs Python3 and MySQL

Install the dependencies on a virtual enviroment:


```
#!python
virtual env
source env/bin/activate

pip install -r requirements
```

You need to setup a MySQL server, create a new DB schema and populate the authors, artists, series, demographies, categories and type tables using the SQL scripts under the Utils folder.

After you've setup the DB and installed the dependencies, cd into the hkmanga app directory and run:


```
#!python

$ python manage.py makemigrations
$ python manage.py migrate
```


Now you can run the server and have full access to the series, authors, artists tables via the API rest.


```
#!python

$ python manage.py runserver
```