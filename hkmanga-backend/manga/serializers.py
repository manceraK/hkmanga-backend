
from rest_framework import serializers, viewsets
from manga.models import *
from rest_framework import filters
from rest_framework_word_filter import FullWordSearchFilter


class DemographySerializer(serializers.ModelSerializer):
    class Meta:
        model = Demography
        fields = '__all__'


class DemographyViewSet(viewsets.ModelViewSet):
    queryset = Demography.objects.all()
    serializer_class = DemographySerializer


class TypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Type
        fields = '__all__'


class TypeViewSet(viewsets.ModelViewSet):
    queryset = Type.objects.all()
    serializer_class = TypeSerializer


class MagazineSerializer(serializers.ModelSerializer):
    class Meta:
        model = Magazine
        fields = '__all__'


class MagazineViewSet(viewsets.ModelViewSet):
    queryset = Magazine.objects.all()
    serializer_class = MagazineSerializer


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'
        # fields = ('id', 'name', 'synopsis', 'start_date', 'status')


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = '__all__'


class AuthorViewSet(viewsets.ModelViewSet):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer


class ArtistSerializer(serializers.ModelSerializer):
    class Meta:
        model = Artist
        fields = '__all__'


class ArtistViewSet(viewsets.ModelViewSet):
    queryset = Artist.objects.all()
    serializer_class = ArtistSerializer


class GenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Genre
        fields = '__all__'


class GenreViewSet(viewsets.ModelViewSet):
    queryset = Genre.objects.all()
    serializer_class = GenreSerializer


class StatsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stats
        fields = '__all__'


class StatsViewSet(viewsets.ModelViewSet):
    queryset = Stats.objects.all()
    serializer_class = StatsSerializer


class SeriesSerializer(serializers.ModelSerializer):
    genres = GenreSerializer(many=True)
    demography = DemographySerializer()
    magazine = MagazineSerializer()
    category = CategorySerializer()
    authors = AuthorSerializer(many=True)
    artists = ArtistSerializer(many=True)
    type = TypeSerializer()
    stats = StatsSerializer(many=True)

    class Meta:
        model = Series
        fields = ('id', 'name', 'alt_names', 'synopsis', 'start_date', 'end_date', 'added_date', 'status', 'adult_content',
                  'uri', 'url', 'slug', 'genres', 'demography', 'type', 'magazine', 'category', 'authors', 'artists', 'stats')


class SeriesViewSet(viewsets.ModelViewSet):
    queryset = Series.objects.all()
    serializer_class = SeriesSerializer
    filter_backends = (filters.DjangoFilterBackend, FullWordSearchFilter)
    word_fields = ('name',)
    filter_fields = ('demography', 'adult_content', 'type', 'genres', 'category', 'authors', 'artists')
