import datetime
from django.db import models
from uuslug import uuslug


class Author(models.Model):
    name = models.CharField(max_length=100)


class Artist(models.Model):
    name = models.CharField(max_length=100)


class Demography(models.Model):
    dsc = models.CharField(max_length=20)


class Type(models.Model):
    dsc = models.CharField(max_length=20)


class Magazine(models.Model):
    dsc = models.CharField(max_length=45)


class Genre(models.Model):
    dsc = models.CharField(max_length=20)


class Category(models.Model):
    dsc = models.CharField(max_length=20)


class Series(models.Model):
    name = models.CharField(max_length=100)
    alt_names = models.CharField(max_length=255, blank=True, null=True)
    synopsis = models.TextField(max_length=1000, default='Sin descripción', null=True, blank=True)
    authors = models.ManyToManyField(Author, related_name='series', blank=True)
    artists = models.ManyToManyField(Artist, related_name='series', blank=True)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    added_date = models.DateTimeField(auto_now_add=True)
    demography = models.ForeignKey(Demography, related_name='series', on_delete=models.PROTECT, null=True, blank=True)
    genres = models.ManyToManyField(Genre, related_name='series')
    status = models.CharField(max_length=20, null=True, blank=True)
    type = models.ForeignKey(Type, related_name='series', on_delete=models.PROTECT, null=True, blank=True)
    magazine = models.ForeignKey(Magazine, related_name='series', on_delete=models.PROTECT, null=True, blank=True)
    category = models.ForeignKey(Category, related_name='series', on_delete=models.PROTECT, null=True, blank=True)
    adult_content = models.BooleanField(default=False)
    uri = models.CharField(max_length=255, null=True, blank=True)
    url = models.URLField(null=True, blank=True)
    slug = models.SlugField()
    external_id = models.IntegerField(null=True, blank=True)

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = uuslug(self.name, instance=self)
        super(Series, self).save(*args, **kwargs)


class Volume(models.Model):
    manga = models.ForeignKey(Series, related_name='volumes', on_delete=models.CASCADE)
    title = models.CharField(max_length=100, default='N/A', null=True, blank=True)
    volume_number = models.IntegerField(default=0, null=True, blank=True)
    chapter_count = models.PositiveIntegerField(default=0, null=True, blank=True)
    publish_date = models.DateField(null=True, blank=True)
    added_date = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    start_chapter = models.IntegerField(default=1)
    end_chapter = models.IntegerField(null=True, blank=True)
    cover_uri = models.CharField(max_length=255, null=True, blank=True)
    cover_url = models.URLField(null=True, blank=True)
    slug = models.SlugField()

    def save(self, *args, **kwargs):
        self.slug = uuslug(self.title, instance=self)
        super(Volume, self).save(*args, **kwargs)


class Chapter(models.Model):
    manga = models.ForeignKey(Series, related_name='chapters', on_delete=models.CASCADE)
    # Check  through attribute foreignkey
    volume = models.ForeignKey(Volume, related_name='chapters', on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    chapter_number = models.PositiveIntegerField()
    publish_date = models.DateField(null=True, blank=True)
    # should we take this out?
    added_date = models.DateTimeField(auto_now_add=True)
    pages_count = models.PositiveIntegerField()
    view_count = models.PositiveIntegerField(default=0)
    slug = models.SlugField()

    def save(self, *args, **kwargs):
        self.slug = uuslug(self.title, instance=self)
        super(Chapter, self).save(*args, **kwargs)


class Scanlation(models.Model):
    name = models.CharField(max_length=255)
    email = models.EmailField(null=True, blank=True)
    avatar_uri = models.CharField(max_length=255, null=True, blank=True)
    avatar_url = models.URLField(null=True, blank=True)
    bio = models.TextField(max_length=1000, null=True, blank=True)
    website = models.URLField(null=True, blank=True)
    facebook = models.URLField(null=True, blank=True)
    twitter = models.URLField(null=True, blank=True)
    google_plus = models.URLField(null=True, blank=True)
    creation_date = models.DateField(auto_now_add=True)
    active = models.BooleanField(default=False)
    banned = models.BooleanField(default=False)
    slug = models.SlugField()

    def save(self, *args, **kwargs):
        self.slug = uuslug(self.name, instance=self)
        super(Scanlation, self).save(*args, **kwargs)


class ChapterDetail(models.Model):
    chapter = models.ForeignKey(Chapter, on_delete=models.CASCADE)
    scanlation = models.ForeignKey(Scanlation, on_delete=models.CASCADE, null=True, blank=True)
    uri = models.CharField(max_length=255, null=True, blank=True)
    url = models.URLField()
    page_number = models.PositiveIntegerField()


class ScanlationAnnouncement(models.Model):
    scanlation = models.ForeignKey(Scanlation, related_name='announcements', on_delete=models.CASCADE)
    publish_date = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=100, default='Title')
    announcement = models.TextField(max_length=1500)
    slug = models.SlugField()

    def save(self, *args, **kwargs):
        self.slug = uuslug(self.title, instance=self)
        super(ScanlationAnnouncement, self).save(*args, **kwargs)


class ScanlationProject(models.Model):
    ACTIVE = 'active'
    COMPLETED = 'finished'
    STALLED = 'stalled'
    DROPPED = 'dropped'

    STATUS = (
        (ACTIVE, 'Activo'),
        (COMPLETED, 'Completado'),
        (STALLED, 'Detendido'),
        (DROPPED, 'Abandonado'),
    )

    scanlation = models.ForeignKey(Scanlation, related_name='projects', on_delete=models.CASCADE)
    serie = models.ForeignKey(Series, related_name='scanlators', on_delete=models.CASCADE)
    status = models.CharField(max_length=9, choices=STATUS, default=ACTIVE)


class Stats(models.Model):
    series = models.ForeignKey(Series, related_name='stats', on_delete=models.CASCADE)
    vote_count = models.IntegerField(default=0)
    total_votes = models.IntegerField(default=0)
    score = models.FloatField(default=0.0)
    total_views = models.IntegerField(default=0)
    followers_count = models.IntegerField(default=0)
