# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-04-15 03:25
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('manga', '0004_auto_20160414_2157'),
    ]

    operations = [
        migrations.CreateModel(
            name='Stats',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('vote_count', models.IntegerField(default=0)),
                ('total_votes', models.IntegerField(default=0)),
                ('ranking', models.FloatField(default=0.0)),
                ('total_views', models.IntegerField(default=0)),
                ('series_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='stats', to='manga.Series')),
            ],
        ),
    ]
