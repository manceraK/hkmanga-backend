# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-04-10 02:14
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Chapter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('chapter_number', models.PositiveIntegerField()),
                ('publish_date', models.DateField()),
                ('added_date', models.DateTimeField(auto_now_add=True)),
                ('pages_count', models.PositiveIntegerField()),
                ('view_count', models.PositiveIntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='ChapterDetail',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uri', models.CharField(max_length=255)),
                ('url', models.URLField()),
                ('page_number', models.PositiveIntegerField()),
                ('chapter', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='manga.Chapter')),
            ],
        ),
        migrations.CreateModel(
            name='Demography',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dsc', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Genre',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dsc', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Magazine',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dsc', models.CharField(max_length=45)),
            ],
        ),
        migrations.CreateModel(
            name='Scanlation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('email', models.EmailField(max_length=254)),
                ('avatar_uri', models.CharField(max_length=255)),
                ('avatar_url', models.URLField()),
                ('bio', models.TextField(max_length=1000)),
                ('website', models.URLField()),
                ('facebook', models.URLField()),
                ('twitter', models.URLField()),
                ('google_plus', models.URLField()),
                ('creation_date', models.DateField()),
                ('active', models.BooleanField()),
                ('banned', models.BooleanField()),
            ],
        ),
        migrations.CreateModel(
            name='ScanlationAnnouncement',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('publish_date', models.DateTimeField()),
                ('announcement', models.TextField(max_length=1500)),
                ('scanlation', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='announcements', to='manga.Scanlation')),
            ],
        ),
        migrations.CreateModel(
            name='ScanlationProject',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.CharField(choices=[('active', 'Activo'), ('finished', 'Completado'), ('stalled', 'Detendido'), ('dropped', 'Abandonado')], default='active', max_length=9)),
                ('scanlation', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='projects', to='manga.Scanlation')),
            ],
        ),
        migrations.CreateModel(
            name='Series',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('alternative_names', models.CharField(max_length=255)),
                ('synopsis', models.TextField(default='Sin descripción', max_length=1000)),
                ('start_date', models.DateField()),
                ('end_date', models.DateField()),
                ('added_date', models.DateTimeField(auto_now_add=True)),
                ('adult_content', models.BooleanField()),
                ('uri', models.CharField(max_length=255)),
                ('url', models.URLField()),
                ('authors', models.ManyToManyField(related_name='series', to='manga.Author')),
                ('demography', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='series', to='manga.Demography')),
                ('magazine', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='series', to='manga.Magazine')),
            ],
        ),
        migrations.CreateModel(
            name='SeriesStatus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dsc', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Type',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dsc', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Volume',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(default='Sin volumen', max_length=100)),
                ('volume_number', models.IntegerField()),
                ('chapter_count', models.PositiveIntegerField()),
                ('publish_date', models.DateField()),
                ('added_date', models.DateTimeField(auto_now_add=True)),
                ('start_chapter', models.IntegerField()),
                ('end_chapter', models.IntegerField()),
                ('cover_uri', models.CharField(max_length=255)),
                ('cover_url', models.URLField()),
                ('manga', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='volumes', to='manga.Series')),
            ],
        ),
        migrations.AddField(
            model_name='series',
            name='status',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='manga.SeriesStatus'),
        ),
        migrations.AddField(
            model_name='series',
            name='type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='series', to='manga.Type'),
        ),
        migrations.AddField(
            model_name='scanlationproject',
            name='serie',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='scanlators', to='manga.Series'),
        ),
        migrations.AddField(
            model_name='chapterdetail',
            name='scanlation',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='manga.Scanlation'),
        ),
        migrations.AddField(
            model_name='chapter',
            name='manga',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='chapters', to='manga.Series'),
        ),
        migrations.AddField(
            model_name='chapter',
            name='volume',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='chapters', to='manga.Volume'),
        ),
    ]
