# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-04-15 02:57
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('manga', '0003_auto_20160411_2052'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dsc', models.CharField(max_length=20)),
            ],
        ),
        migrations.RemoveField(
            model_name='series',
            name='alternative_names',
        ),
        migrations.AddField(
            model_name='series',
            name='alt_names',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='series',
            name='external_id',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='chapter',
            name='publish_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='chapter',
            name='view_count',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='chapterdetail',
            name='scanlation',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='manga.Scanlation'),
        ),
        migrations.AlterField(
            model_name='chapterdetail',
            name='uri',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='scanlation',
            name='active',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='scanlation',
            name='avatar_uri',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='scanlation',
            name='avatar_url',
            field=models.URLField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='scanlation',
            name='banned',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='scanlation',
            name='bio',
            field=models.TextField(blank=True, max_length=1000, null=True),
        ),
        migrations.AlterField(
            model_name='scanlation',
            name='creation_date',
            field=models.DateField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='scanlation',
            name='email',
            field=models.EmailField(blank=True, max_length=254, null=True),
        ),
        migrations.AlterField(
            model_name='scanlation',
            name='facebook',
            field=models.URLField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='scanlation',
            name='google_plus',
            field=models.URLField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='scanlation',
            name='twitter',
            field=models.URLField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='scanlation',
            name='website',
            field=models.URLField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='scanlationannouncement',
            name='publish_date',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='series',
            name='adult_content',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='series',
            name='artists',
            field=models.ManyToManyField(blank=True, related_name='series', to='manga.Artist'),
        ),
        migrations.AlterField(
            model_name='series',
            name='authors',
            field=models.ManyToManyField(blank=True, related_name='series', to='manga.Author'),
        ),
        migrations.AlterField(
            model_name='series',
            name='demography',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='series', to='manga.Demography'),
        ),
        migrations.AlterField(
            model_name='series',
            name='end_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='series',
            name='magazine',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='series', to='manga.Magazine'),
        ),
        migrations.AlterField(
            model_name='series',
            name='start_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='series',
            name='status',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='series',
            name='synopsis',
            field=models.TextField(blank=True, default='Sin descripción', max_length=1000, null=True),
        ),
        migrations.AlterField(
            model_name='series',
            name='type',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='series', to='manga.Type'),
        ),
        migrations.AlterField(
            model_name='series',
            name='uri',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='series',
            name='url',
            field=models.URLField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='volume',
            name='added_date',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AlterField(
            model_name='volume',
            name='chapter_count',
            field=models.PositiveIntegerField(blank=True, default=0, null=True),
        ),
        migrations.AlterField(
            model_name='volume',
            name='cover_uri',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='volume',
            name='cover_url',
            field=models.URLField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='volume',
            name='end_chapter',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='volume',
            name='publish_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='volume',
            name='start_chapter',
            field=models.IntegerField(default=1),
        ),
        migrations.AlterField(
            model_name='volume',
            name='title',
            field=models.CharField(blank=True, default='N/A', max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='volume',
            name='volume_number',
            field=models.IntegerField(blank=True, default=0, null=True),
        ),
        migrations.AddField(
            model_name='series',
            name='category',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='series', to='manga.Category'),
        ),
    ]
