# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-04-15 03:30
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('manga', '0005_stats'),
    ]

    operations = [
        migrations.RenameField(
            model_name='stats',
            old_name='series_id',
            new_name='series',
        ),
    ]
